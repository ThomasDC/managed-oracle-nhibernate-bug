﻿using System;
using System.Linq;
using NHibernate.Cfg;

namespace ManagedOracleNHibernateBatchUpdateBug
{
    class Program
    {
        static void Main(string[] args)
        {
            var sessionFactory = new Configuration().Configure().BuildSessionFactory();

            var html = string.Join("\n", Enumerable.Range(0, 80).Select(_ => "<div>Foobar</div>"));

            using (var session = sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var bug = new Bug
                    {
                        Id = 1,
                        Content = html
                    };

                    session.Save(bug);

                    // Code will crash here! It will work when you remove 'adonet.batch_size' from App.config
                    transaction.Commit();
                }
            }
        }
    }
}

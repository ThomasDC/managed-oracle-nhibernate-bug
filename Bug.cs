﻿namespace ManagedOracleNHibernateBatchUpdateBug
{
    public class Bug
    {
        public virtual long Id { get; set; }
        public virtual string Content { get; set; }
    }
}
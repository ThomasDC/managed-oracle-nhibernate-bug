# Reproduces a potential bug in the Managed Oracle Data Access Driver #

Following technologies were used:

* .NET 4.5.2
* Oracle.ManagedDataAccess version 12.1.2400
* NHibernate version 4.0.4.4000
* Oracle Database 11g Release 11.2.0.4.0

When running this sample application, you will get an 'ORA-01483'.

Removing `adonet.batch_size` seems to solve the problem.

Related issues:

- https://community.oracle.com/thread/3649551?start=0&tstart=0
- https://community.oracle.com/thread/3640248?start=0&tstart=0